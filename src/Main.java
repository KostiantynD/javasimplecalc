import syntaxanalyzer.Expr;
import syntaxanalyzer.SyntaxBTR;
import lexanalyzer.LexAnalyzer;
import lexanalyzer.Lexeme;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.flush();
        System.out.println("\n\t\t -+- Grammar in simple BNF -+-  \n");
        System.out.println("\t <expr>       :: <add>");
        System.out.println("\t <add>        :: <mult>  [ ( + , - )  <mult>   ]*  ");
        System.out.println("\t <mult>       :: <group> [ ( * , / )  <group>  ]*  ");
        System.out.println("\t <group>      :: <NUM> | <LBR> <expr> <RBR>  ");
        System.out.println("\t <LBR>        :: '(' ");
        System.out.println("\t <RBR>        :: ')' ");
        System.out.println("\t <NUM>        :: ( '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '0')+  " );
        System.out.println("\t Could be added negative numbers and double type   \n");

        System.out.println("Alphabet: 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, +, -, *, /, (, )  \n");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("Waiting for expression or 'q' for exit:  \n");
            try {
                String input = scanner.nextLine();
                if (input.charAt(0) == 'q'){
                    break;
                }

                LexAnalyzer reader = new LexAnalyzer(input);
                List<Lexeme> gotLexemes = reader.getLexemes();

                for (Lexeme lexeme : gotLexemes)
                     System.out.println(lexeme);

                Expr expr = new SyntaxBTR(gotLexemes).parse();
                System.out.println("parsed expression: " + expr);
                System.out.println("Total: " + expr.calculate());
            } catch (Exception ex) {
                System.out.println("Raised computational or input error");
            }
        }
    }
}