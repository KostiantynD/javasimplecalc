package lexanalyzer;

import java.util.ArrayList;
import java.util.List;

import static lexanalyzer.LexemeType.*;

public class LexAnalyzer {
    private final String input;
    private int start;
    private int current;

    private final List<Lexeme> gotLexemes;

    public LexAnalyzer(String input) {
        this.input = input;
        this.start = 0;
        this.current = 0;
        this.gotLexemes = new ArrayList<>();
    }

    public List<Lexeme> getLexemes() {
        while (!isAtEnd()) {
            start = current;
            getLexeme();
        }
        return gotLexemes;
    }

    private void getLexeme() {
        char ch = takeNextChar();

        switch (ch) {
            case ' ' :
            case '\n':
                break;
            case '(':
                addLexeme(LBR); break;
            case ')':
                addLexeme(RBR); break;
            case '+':
                addLexeme(ADD); break;
            case '-':
                addLexeme(SUB); break;
            case '*':
                addLexeme(MULT); break;
            case '/':
                addLexeme(DIV); break;
            default:
                if (Character.isDigit(ch)) {
                    addNumberLexeme();
                }
        }
    }

    private boolean isAtEnd() {
        return current >= input.length();
    }

    private char takeNextChar() {
        return input.charAt(current++);
    }

    private char takeCurrentChar() {
        return input.charAt(current);
    }

    private void addNumberLexeme() {
        while (!isAtEnd() && Character.isDigit(takeCurrentChar())) {
            takeNextChar();
        }
        addLexeme(NUM, Integer.parseInt(getCurrentSubstring()));
    }

    private void addLexeme(LexemeType type) {
        addLexeme(type, null);
    }

    private void addLexeme(LexemeType type, Object value) {
        gotLexemes.add(newLexeme(type, value));
    }

    private Lexeme newLexeme(LexemeType type, Object value) {
        return new Lexeme(type, getCurrentSubstring(), value);
    }

    private String getCurrentSubstring() {
        return input.substring(start, current);
    }
}

