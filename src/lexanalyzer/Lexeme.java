package lexanalyzer;

public class Lexeme {

    private final LexemeType type;
    private final String lexeme;
    private final Object value;

    public Lexeme(LexemeType type, String lexeme, Object value) {
        this.type = type;
        this.lexeme = lexeme;
        this.value = value;
    }

    public LexemeType getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "type = " + type + ", lexeme = " + lexeme;
    }
}