package syntaxanalyzer;
import lexanalyzer.Lexeme;
import lexanalyzer.LexemeType;
import java.util.List;
import java.util.function.Supplier;
import static lexanalyzer.LexemeType.*;

public class SyntaxBTR {

    private final List<Lexeme> lexemes;
    private int current;

    public SyntaxBTR(List<Lexeme> lexemes) {
        this.lexemes = lexemes;
        this.current = 0;
    }

    public Expr parse() {
        return add();
    }

//    <add> ::= <mult>  [ ( +, - ) <mult> ]*
    private Expr add() {
        return parseBinary(this::mult, this::mult, ADD, SUB);
    }

//     <mult> ::= <group> [ ( * , / ) <group> ]*
    private Expr mult() {
        return parseBinary(this::group, this::group, MULT, DIV);
    }

//     <group> ::= <NUM> | ( <expr> )
    private Expr group() {
        if (match(NUM)) {
            return new Expr.LexemeNumber(previous());
        }
        if (match(LBR)) {
            Expr expr = parse();

            if (!match(RBR)) {
                throw new RuntimeException("Closing bracket = ')' is absent !!!.");
            }
            return new Expr.GroupingExpr(expr);
        }
        throw new RuntimeException("Expected number or open parenthesis, found " + getCurrLexeme());
    }


    private Expr parseBinary(Supplier<Expr> initialExpression, Supplier<Expr> repeatingExpression, LexemeType... types) {
        Expr expr = initialExpression.get();

        while (match(types)) {
            if (isEndLexemesList()) {
              throw new RuntimeException("Missing Parent for binary childs.");
            }
            expr = new Expr.Binary(expr, previous(), repeatingExpression.get());
        }
        return expr;
    }

    private boolean isEndLexemesList() {
        return current >= lexemes.size();
    }

    private Lexeme getNextLexeme() {
        return lexemes.get(current++);
    }

    private Lexeme getCurrLexeme() {
        return lexemes.get(current);
    }

    private Lexeme previous() {
        return lexemes.get(current - 1);
    }

    private boolean match(LexemeType... types) {
        for (LexemeType type : types) {
            if (check(type)) {
                getNextLexeme();
                return true;
            }
        }
        return false;
    }

    private boolean check(LexemeType type) {
        if (isEndLexemesList())
            return false;
        return getCurrLexeme().getType() == type;
    }
}