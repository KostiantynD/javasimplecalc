package syntaxanalyzer;
import lexanalyzer.Lexeme;

public interface Expr {
    int calculate();


    class Binary implements Expr {
        final Expr leftNode, rightNode;  //leafs
        final Lexeme operand; //root and internal nodes

        public Binary(Expr leftNode, Lexeme operand, Expr rightNode) {
            this.leftNode = leftNode;
            this.rightNode = rightNode;
            this.operand = operand;
        }

        @Override
        public int calculate() {
            int leftNodeValue = leftNode.calculate();
            int rightNodeValue = rightNode.calculate();

            switch (operand.getType()) {
                case ADD:
                    return leftNodeValue + rightNodeValue;
                case SUB:
                    return leftNodeValue - rightNodeValue;
                case MULT:
                    return leftNodeValue * rightNodeValue;
                case DIV:
                    return leftNodeValue / rightNodeValue;
                default:
                    throw new RuntimeException("Unknown binary operator: " + operand);
            }
        }

        @Override
        public String toString() {
            return "[ " + leftNode.toString() + ' ' + operand.getType() + ' ' + rightNode.toString() + " ]";
        }
    }

    class GroupingExpr implements Expr {
        private final Expr expr;

        public GroupingExpr(Expr expr) {
            this.expr = expr;
        }

        @Override
        public int calculate() {
            return expr.calculate();
        }

        @Override
        public String toString() {
            return '(' + expr.toString() + ')';
        }
    }

    class LexemeNumber implements Expr {
        final Lexeme lexemeNumber;

        public LexemeNumber(Lexeme lexeme) {
            this.lexemeNumber = lexeme;
        }

        @Override
        public int calculate() {
            return (int) lexemeNumber.getValue();
        }

        @Override
        public String toString() {
            return ((Integer) lexemeNumber.getValue()).toString();
        }

    }
}